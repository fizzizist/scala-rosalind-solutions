package rna

import scala.io.Source

object IPRB {
  def main(args: Array[String]) = {
    val popul = args.map(_.toDouble)
    // algorithm just assumes the following inputs
    // popul(0) = homozygous dominant
    // popul(1) = heterozygous
    // popul(2) = homozygous recessive
    val total = popul.sum
    // case of both homozygous recessive
    var probabTotal = (popul(2) / total) * ((popul(2) - 1) / (total - 1))
    // case of heterozygous and homozygous recessive mix
    probabTotal += ((popul(2) / total) * (popul(1) / (total - 1))) * 0.5
    // case of homozygous recessive and heterozygous mix
    probabTotal += ((popul(1) / total) * (popul(2) / (total - 1))) * 0.5
    // case of both heterozygous
    probabTotal += ((popul(1) / total) * ((popul(1) - 1) / (total - 1))) * 0.25
    probabTotal = 1 - probabTotal
    println(f"$probabTotal%.5f")
  }
}
