package gc

import scala.io.Source
import java.io._


class GCCalculator() {
  var dnaStr = ""
  var topContent = 0.0
  var topDNA = ""
  var currentDNA = ""

  def checkCurrDNA() {
    val gcCheck = (c: Char) => (c == 'G' || c == 'C')
    val gcContent = dnaStr.filter(gcCheck).length
    val contentPerc = (gcContent.toFloat / dnaStr.length.toFloat) * 100.0
    if (contentPerc > topContent) {
      topContent = contentPerc
      topDNA = currentDNA
    }
  }

  def printTopGC() {
    val filename = "GC.txt"
    for (line <- Source.fromFile(filename).getLines) {
      if (line.startsWith(">")) {
        if (dnaStr.length > 0) {
          checkCurrDNA
        }
        currentDNA = line
        dnaStr = ""
      } else {
        dnaStr += line
      }
    }
    checkCurrDNA
    println(topDNA)
    println(f"$topContent%.6f")
  }
}


object GC {
  def main(args: Array[String]) = {
    val gcCalc = new GCCalculator
    gcCalc.printTopGC
  }
}
