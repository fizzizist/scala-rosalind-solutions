package hamm

import scala.io.Source
import java.io._

object HAMM {
  def main(args: Array[String]) = {
    val filename = "HAMM.txt"
    val dna = Source.fromFile(filename).getLines.toList
    var count = 0
    for ((i, j) <- (dna(0), dna(1)).zipped) {
      if (i != j) {
        count += 1
      }
    }
    println(count)
  }
}
