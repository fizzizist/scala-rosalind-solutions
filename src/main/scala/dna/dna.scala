package dna

import scala.io.Source

object DNA {
  def main(args: Array[String]) = {
    val filename = "DNA.txt"
    val dnaStr = Source.fromFile(filename).getLines.mkString
    val acgt = Array(0, 0, 0, 0)
    val acgtMap = Map('A' -> 0, 'C' -> 1, 'G' -> 2, 'T' -> 3)
    for (c <- dnaStr) acgt(acgtMap(c)) += 1
    println("%d %d %d %d".format(acgt(0), acgt(1), acgt(2), acgt(3)))
  }
}
