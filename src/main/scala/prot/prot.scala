package prot

import scala.io.Source
import java.io._

object PROT {
  def main(args: Array[String]) = {
    val filename = "PROT.txt"
    val rnaStr = Source.fromFile(filename).getLines.mkString
    val transMap = Map(
      "UUU" -> "F",
      "CUU" -> "L",
      "AUU" -> "I",
      "GUU" -> "V",
      "UUC" -> "F",
      "CUC" -> "L",
      "AUC" -> "I",
      "GUC" -> "V",
      "UUA" -> "L",
      "CUA" -> "L",
      "AUA" -> "I",
      "GUA" -> "V",
      "UUG" -> "L",
      "CUG" -> "L",
      "AUG" -> "M",
      "GUG" -> "V",
      "UCU" -> "S",
      "CCU" -> "P",
      "ACU" -> "T",
      "GCU" -> "A",
      "UCC" -> "S",
      "CCC" -> "P",
      "ACC" -> "T",
      "GCC" -> "A",
      "UCA" -> "S",
      "CCA" -> "P",
      "ACA" -> "T",
      "GCA" -> "A",
      "UCG" -> "S",
      "CCG" -> "P",
      "ACG" -> "T",
      "GCG" -> "A",
      "UAU" -> "Y",
      "CAU" -> "H",
      "AAU" -> "N",
      "GAU" -> "D",
      "UAC" -> "Y",
      "CAC" -> "H",
      "AAC" -> "N",
      "GAC" -> "D",
      "UAA" -> "Stop",
      "CAA" -> "Q",
      "AAA" -> "K",
      "GAA" -> "E",
      "UAG" -> "Stop",
      "CAG" -> "Q",
      "AAG" -> "K",
      "GAG" -> "E",
      "UGU" -> "C",
      "CGU" -> "R",
      "AGU" -> "S",
      "GGU" -> "G",
      "UGC" -> "C",
      "CGC" -> "R",
      "AGC" -> "S",
      "GGC" -> "G",
      "UGA" -> "Stop",
      "CGA" -> "R",
      "AGA" -> "R",
      "GGA" -> "G",
      "UGG" -> "W",
      "CGG" -> "R",
      "AGG" -> "R",
      "GGG" -> "G",
    )
    val rnaSegs = rnaStr.grouped(3).toList
    var protStr = ""
    rnaSegs.iterator.takeWhile(transMap(_) != "Stop").foreach(protStr += transMap(_))
    println(protStr)
    val pw = new PrintWriter(new File("PROTOut.txt"))
    pw.write(protStr)
    pw.close()
  }
}
