package rna

import scala.io.Source
import java.io._

object RNA {
  def main(args: Array[String]) = {
    val filename = "RNA.txt"
    val dnaStr = Source.fromFile(filename).getLines.mkString
    val rnaStr = dnaStr.replace('T', 'U')
    val pw = new PrintWriter(new File("RNAOut.txt"))
    pw.write(rnaStr)
    pw.close()
  }
}
