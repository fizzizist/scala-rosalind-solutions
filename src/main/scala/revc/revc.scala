package rna

import scala.io.Source
import java.io._

object RNA {
  def main(args: Array[String]) = {
    val filename = "REVC.txt"
    val dnaStr = Source.fromFile(filename).getLines.mkString
    val compMap = Map('A' -> 'T', 'T' -> 'A', 'C' -> 'G', 'G' -> 'C')
    val comp = dnaStr.reverse.map(compMap(_))
    val pw = new PrintWriter(new File("REVCOut.txt"))
    pw.write(comp)
    pw.close()
  }
}
