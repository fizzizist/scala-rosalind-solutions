package fib

import scala.io.Source

object FIB {
  def main(args: Array[String]) = {
    val n = args(0).toInt
    val k = args(1).toInt
    var b = BigInt(0)
    var a = BigInt(1)
    var newA = BigInt(0)
    var i = 0
    for (i <- 2 until n) {
      a += newA
      newA = b
      b = a*k
    }
    val res = a + b + newA
    println(res.toString)
  }
}
