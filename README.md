# Scala Rosalind Solutions
A set of solutions to the popular [rosalind.info](https://www.rosalind.info) bioinformatics programming puzzle site.
